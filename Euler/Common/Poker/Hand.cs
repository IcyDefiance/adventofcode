﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Euler.Common.Poker
{
    class Hand : IComparable
    {
        readonly Card[] cards = new Card[5];

        public Hand(IEnumerable<Card> cards)
        {
            if (cards.Count() != 5)
                throw new ArgumentException("There must be 5 cards");

            this.cards = cards.OrderByDescending(x => x.Value).ToArray();
        }

        int CompareIndividual(Hand other)
        {
            for (int i = 0; i < 5; i++)
                if (this.cards[i].Value > other.cards[i].Value)
                    return 1;
                else if (this.cards[i].Value < other.cards[i].Value)
                    return -1;

            return 0;
        }

        public int CompareTo(object obj)
        {
            Hand other = obj as Hand;
            if (other == null)
                throw new ArgumentException("Parameter is not a Hand");

            HandRank lrank = this.Rank();
            HandRank rrank = other.Rank();

            if (lrank > rrank)
            {
                return 1;
            }
            else if (lrank == rrank)
            {
                if (lrank == HandRank.HighCard || lrank == HandRank.Straight || lrank == HandRank.Flush || lrank == HandRank.StraightFlush)
                {
                    return CompareIndividual(other);
                }
                else if (lrank == HandRank.Pair)
                {
                    int tpv = this.PairValue();
                    int opv = other.PairValue();
                    if (tpv > opv)
                        return 1;
                    else if (tpv < opv)
                        return -1;
                    else
                        return CompareIndividual(other);
                }
                else if (lrank == HandRank.TwoPair)
                {
                    int lp1 = this.PairValue();
                    int lp2 = this.PairValue(lp1);
                    int rp1 = other.PairValue();
                    int rp2 = other.PairValue(rp1);

                    int p1comp = lp1.CompareTo(rp1);
                    int p2comp = lp2.CompareTo(rp2);

                    if (p1comp != 0)
                        return p1comp;
                    else if (p2comp != 0)
                        return p2comp;
                    else
                        return CompareIndividual(other);
                }
                else if (lrank == HandRank.ThreeOfAKind)
                {
                    int tvcomp = this.ThreeValue().CompareTo(other.ThreeValue());
                    if (tvcomp != 0)
                        return tvcomp;
                    else
                        return CompareIndividual(other);
                }
                else if (lrank == HandRank.FullHouse)
                {
                    int tvcomp = this.ThreeValue().CompareTo(other.ThreeValue());
                    int pvcomp = this.PairValue().CompareTo(other.PairValue());
                    if (tvcomp != 0)
                        return tvcomp;
                    else
                        return pvcomp;
                }
                else if (lrank == HandRank.FourOfAKind)
                {
                    int fvcomp = this.FourValue().CompareTo(other.FourValue());
                    if (fvcomp != 0)
                        return fvcomp;
                    else
                        return CompareIndividual(other);
                }
                else //if (lrank == HandRank.RoyalFlush)
                {
                    return 0;
                }
            }
            else
            {
                return -1;
            }
        }

        public static bool operator >(Hand left, Hand right)
        {
            return left.CompareTo(right) > 0;
        }

        public static bool operator <(Hand left, Hand right)
        {
            return left.CompareTo(right) < 0;
        }

        HandRank Rank()
        {
            return IsRoyalFlush() ? HandRank.RoyalFlush :
                IsStraightFlush() ? HandRank.StraightFlush :
                IsFourOfAKind() ? HandRank.FourOfAKind :
                IsFullHouse() ? HandRank.FullHouse :
                IsFlush() ? HandRank.Flush :
                IsStraight() ? HandRank.Straight :
                IsThreeOfAKind() ? HandRank.ThreeOfAKind :
                IsTwoPair() ? HandRank.TwoPair :
                IsPair() ? HandRank.Pair :
                HandRank.HighCard;
        }

        bool IsRoyalFlush()
        {
            return IsFlush() &&
                cards.Any(x => x.Value == 10) &&
                cards.Any(x => x.Value == 11) &&
                cards.Any(x => x.Value == 12) &&
                cards.Any(x => x.Value == 13) &&
                cards.Any(x => x.Value == 14);
        }

        bool IsStraightFlush()
        {
            return IsStraight() && IsFlush();
        }

        bool IsFourOfAKind()
        {
            return cards.Count(x => x.Value == cards[0].Value) == 4 ||
                cards.Count(x => x.Value == cards[1].Value) == 4;
        }

        int FourValue()
        {
            return cards.Count(x => x.Value == cards[0].Value) == 4 ? cards[0].Value :
                cards.Count(x => x.Value == cards[1].Value) == 4 ? cards[1].Value : 0;
        }

        bool IsFullHouse()
        {
            return IsThreeOfAKind() && IsPair();
        }

        bool IsFlush()
        {
            return cards.All(x => x.Suit == cards[0].Suit);
        }

        bool IsStraight()
        {
            int lowValue = cards.Select(x => x.Value).Min();
            return cards.Any(x => x.Value == lowValue + 1) &&
                cards.Any(x => x.Value == lowValue + 2) &&
                cards.Any(x => x.Value == lowValue + 3) &&
                cards.Any(x => x.Value == lowValue + 4);
        }

        bool IsThreeOfAKind()
        {
            return cards.Count(x => x.Value == cards[0].Value) == 3 ||
                cards.Count(x => x.Value == cards[1].Value) == 3 ||
                cards.Count(x => x.Value == cards[2].Value) == 3;
        }

        int ThreeValue()
        {
            return cards.Count(x => x.Value == cards[0].Value) == 3 ? cards[0].Value :
                cards.Count(x => x.Value == cards[1].Value) == 3 ? cards[1].Value :
                cards.Count(x => x.Value == cards[2].Value) == 3 ? cards[2].Value : 0;
        }

        bool IsTwoPair()
        {
            int p1 = PairValue();
            return p1 > 0 && PairValue(p1) > 0;
        }

        bool IsPair()
        {
            return cards.Count(x => x.Value == cards[0].Value) == 2 ||
                cards.Count(x => x.Value == cards[1].Value) == 2 ||
                cards.Count(x => x.Value == cards[2].Value) == 2 ||
                cards.Count(x => x.Value == cards[3].Value) == 2;
        }

        int PairValue(int except = -1)
        {
            return cards[0].Value != except && cards.Count(x => x.Value == cards[0].Value) == 2 ? cards[0].Value :
                cards[1].Value != except && cards.Count(x => x.Value == cards[1].Value) == 2 ? cards[1].Value :
                cards[2].Value != except && cards.Count(x => x.Value == cards[2].Value) == 2 ? cards[2].Value :
                cards[3].Value != except && cards.Count(x => x.Value == cards[3].Value) == 2 ? cards[3].Value : 0;
        }

        enum HandRank
        {
            HighCard,
            Pair,
            TwoPair,
            ThreeOfAKind,
            Straight,
            Flush,
            FullHouse,
            FourOfAKind,
            StraightFlush,
            RoyalFlush
        }
    }
}
