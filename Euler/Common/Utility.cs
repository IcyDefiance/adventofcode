﻿using System;
using System.Linq;
using System.Numerics;

namespace Euler.Common
{
    class Utility
    {
        public static bool IsPermutation(string left, string right)
        {
            for (int i = 0; i < left.Length; i++)
            {
                int ri = right.IndexOf(left[i]);
                if (ri > -1)
                    right = right.Remove(ri, 1);
                else
                    return false;
            }

            if (right.Length > 0)
                return false;

            return true;
        }

        public static bool IsPalindrome(string str)
        {
            return str.Reverse().SequenceEqual(str);
        }

        public static BigInteger XtakeY(BigInteger x, BigInteger y)
        {
            if (y > x)
                throw new ArgumentException("Can't take that many from x");

            return x.Factorial() / (y.Factorial() * (x - y).Factorial());
        }
    }
}
