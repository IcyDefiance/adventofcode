﻿using System;
using System.IO;
using System.Linq;

namespace Euler.Solutions
{
    class P2
    {
        public static void Run()
        {
            var input = File.ReadAllLines("Resources/p2input.txt");
            Console.WriteLine(input.Sum(x =>
            {
                var split = x.Split('x').Select(y => int.Parse(y)).ToArray();
                return 2 * split[0] * split[1] +
                       2 * split[1] * split[2] +
                       2 * split[2] * split[0] +
                       Math.Min(Math.Min(
                           split[0] * split[1],
                           split[1] * split[2]),
                           split[2] * split[0]
                       );
            }));

            Console.WriteLine(input.Sum(x =>
            {
                var split = x.Split('x').Select(y => int.Parse(y)).ToArray();
                return split[0] * split[1] * split[2] +
                       2 * Math.Min(Math.Min(
                           split[0] + split[1],
                           split[1] + split[2]),
                           split[2] + split[0]
                       );
            }));
        }
    }
}
