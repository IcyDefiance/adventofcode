﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Euler.Solutions
{
    class P7
    {
        static Dictionary<string, Func<int>> wires = new Dictionary<string, Func<int>>();
        static Dictionary<string, int> signals = new Dictionary<string, int>();

        public static void Run()
        {
            var input = File.ReadAllLines("Resources/p7input.txt");

            foreach (var line in input)
            {
                var words = line.Split(new[] { " -> ", " " }, StringSplitOptions.None);
                
                if (words.Length == 2)
                    wires[words[1]] = () => { return GetSignal(words[0]); };
                else if (words[0] == "NOT")
                    wires[words[2]] = () => { return ~GetSignal(words[1]); };
                else if (words[1] == "AND")
                    wires[words[3]] = () => { return GetSignal(words[0]) & GetSignal(words[2]); };
                else if (words[1] == "OR")
                    wires[words[3]] = () => { return GetSignal(words[0]) | GetSignal(words[2]); };
                else if (words[1] == "LSHIFT")
                    wires[words[3]] = () => { return GetSignal(words[0]) << ushort.Parse(words[2]); };
                else if (words[1] == "RSHIFT")
                    wires[words[3]] = () => { return GetSignal(words[0]) >> ushort.Parse(words[2]); };
                else
                    throw new InvalidOperationException("Command not recognized");
            }

            var answer1 = GetSignal("a");
            Console.WriteLine(answer1);

            wires["b"] = () => { return answer1; };
            signals = new Dictionary<string, int>();
            Console.WriteLine(GetSignal("a"));
        }
        
        static int GetSignal(string value)
        {
            ushort num;
            int ret;
            if (signals.TryGetValue(value, out ret))
                return ret;
            else if (ushort.TryParse(value, out num))
                return signals[value] = num;
            else
                return signals[value] = wires[value]();
        }
    }
}
