﻿using System;
using System.IO;
using System.Linq;

namespace Euler.Solutions
{
    class P6
    {
        public static void Run()
        {
            var input = File.ReadAllLines("Resources/p6input.txt")
                .Select(line =>
                {
                    var words = line.Split(new[] { ' ', ',' });
                    return new {
                        Switch = words[1],
                        StartX = int.Parse(words[2]),
                        StartY = int.Parse(words[3]),
                        EndX = int.Parse(words[5]),
                        EndY = int.Parse(words[6])
                    };
                });

            var lights = Enumerable.Range(0, 1000).Select(x => new bool[1000]).ToArray();
            Action<int, int> on = (x, y) => lights[x][y] = true;
            Action<int, int> off = (x, y) => lights[x][y] = false;
            Action<int, int> toggle = (x, y) => lights[x][y] = !lights[x][y];
            foreach (var line in input)
            {
                Action<int, int> op = null;
                if (line.Switch == "on")
                    op = on;
                else if (line.Switch == "off")
                    op = off;
                else if (line.Switch == "toggle")
                    op = toggle;

                for (int x = line.StartX; x <= line.EndX; x++)
                    for (int y = line.StartY; y <= line.EndY; y++)
                        op(x, y);
            }

            Console.WriteLine(lights.Sum(x => x.Count(y => y)));

            var brights = Enumerable.Range(0, 1000).Select(x => new int[1000]).ToArray();
            Action<int, int> add = (x, y) => brights[x][y]++;
            Action<int, int> sub = (x, y) => brights[x][y] = Math.Max(0, brights[x][y] - 1);
            Action<int, int> add2 = (x, y) => brights[x][y] += 2;
            foreach (var line in input)
            {
                Action<int, int> op = null;
                if (line.Switch == "on")
                    op = add;
                else if (line.Switch == "off")
                    op = sub;
                else if (line.Switch == "toggle")
                    op = add2;

                for (int x = line.StartX; x <= line.EndX; x++)
                    for (int y = line.StartY; y <= line.EndY; y++)
                        op(x, y);
            }

            Console.WriteLine(brights.Sum(x => x.Sum(y => y)));
        }
    }
}
