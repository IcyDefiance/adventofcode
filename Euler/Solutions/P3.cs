﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Euler.Solutions
{
    class P3
    {
        public static void Run()
        {
            var input = File.ReadAllText("Resources/p3input.txt");

            var houses = new Dictionary<int, Dictionary<int, int>>() { { 0, new Dictionary<int, int>() { { 0, 1 } } } };
            int x = 0, y = 0;
            foreach (var dir in input)
            {
                if (dir == '^')
                    y--;
                else if (dir == 'v')
                    y++;
                else if (dir == '<')
                    x--;
                else if (dir == '>')
                    x++;

                if (!houses.ContainsKey(x))
                    houses[x] = new Dictionary<int, int>();
                houses[x][y] = 1;
            }
            Console.WriteLine(houses.Sum(h => h.Value.Count));
            
            houses = new Dictionary<int, Dictionary<int, int>>() { { 0, new Dictionary<int, int>() { { 0, 1 } } } };
            x = y = 0;
            int x2 = 0, y2 = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (i % 2 == 0)
                {
                    if (input[i] == '^')
                        y--;
                    else if (input[i] == 'v')
                        y++;
                    else if (input[i] == '<')
                        x--;
                    else if (input[i] == '>')
                        x++;

                    if (!houses.ContainsKey(x))
                        houses[x] = new Dictionary<int, int>();
                    houses[x][y] = 1;
                }
                else
                {
                    if (input[i] == '^')
                        y2--;
                    else if (input[i] == 'v')
                        y2++;
                    else if (input[i] == '<')
                        x2--;
                    else if (input[i] == '>')
                        x2++;

                    if (!houses.ContainsKey(x2))
                        houses[x2] = new Dictionary<int, int>();
                    houses[x2][y2] = 1;
                }

            }
            Console.WriteLine(houses.Sum(h => h.Value.Count));
        }
    }
}
