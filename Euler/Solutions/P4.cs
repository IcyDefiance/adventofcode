﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Euler.Solutions
{
    class P4
    {
        public static void Run()
        {
            string input = "yzbqklnj";
            var md5 = MD5.Create();

            bool five = false, six = false;
            for (int i = 0; !(five && six); i++)
            {
                var hash = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes($"{input}{i}"))).Replace("-", "");
                if (!five && hash.Substring(0, 5) == "00000")
                {
                    Console.WriteLine($"five zeroes at {i}");
                    five = true;
                }
                if (hash.Substring(0, 6) == "000000")
                {
                    Console.WriteLine($"six zeroes at {i}");
                    six = true;
                }
            }
        }
    }
}
