﻿using System;
using System.IO;
using System.Linq;

namespace Euler.Solutions
{
    class P1
    {
        public static void Run()
        {
            var input = File.ReadAllText("Resources/input.txt");
            Console.WriteLine(
                input.Sum(x => x == '(' ? 1 : -1)
            );

            var partsum = 0;
            Console.WriteLine(
                input.TakeWhile(x => (x == '(' ? ++partsum : --partsum) >= -1).Count()
            );
        }
    }
}
