﻿using System;
using System.IO;
using System.Linq;

namespace Euler.Solutions
{
    class P5
    {
        public static void Run()
        {
            var input = File.ReadAllLines("Resources/p5input.txt");

            Console.WriteLine(
                input.Where(line =>
                        line.Select((c, i) => i > 0 && c == line[i - 1]).Any(x => x) &&
                        line.Count(c => "aeiou".Contains(c)) >= 3 &&
                        !line.Contains("ab") && !line.Contains("cd") && !line.Contains("pq") && !line.Contains("xy"))
                    .Count()
            );

            Console.WriteLine(
                input.Where(line =>
                        line.Select((c, i) => i > 0 && line.Substring(i + 1).Contains($"{line[i - 1]}{c}")).Any(x => x) &&
                        line.Select((c, i) => i > 1 && c == line[i - 2]).Any(x => x))
                    .Count()
            );
        }
    }
}
