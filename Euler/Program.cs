﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace Euler
{
    class Program
    {
        const int probCount = 25;

        static void Main(string[] args)
        {
            if (ParseCommand(args))
                return;
            else
            {
                while (true)
                {
                    Console.Write("> ");
                    ParseCommand(Console.ReadLine().Split(' '));
                }
            }
        }

        private static bool ParseCommand(string[] command)
        {
            if (command == null || command.Length == 0)
            {
                Console.WriteLine("Type 'help' for a list of commands");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(command[0]))
                return false;
                
            string commandLower = command[0].ToLower();
            if (commandLower == "help" && command.Length == 1)
                Help();
            else if (commandLower == "list" && command.Length == 1)
                List();
            else if (commandLower == "run" && command.Length == 2)
                Run(command[1]);
            else
            {
                Console.WriteLine("Unrecognized command");
                return false;
            }

            return true;
        }

        private static void Run(string probNum)
        {
            Type probType = Assembly.GetExecutingAssembly().GetType("Euler.Solutions.P" + probNum);
            if (probType != null)
            {
                Action probAction = (Action)Delegate.CreateDelegate(typeof(Action), probType.GetMethod("Run"));
                var watch = Stopwatch.StartNew();
                probAction();
                watch.Stop();
                ConsoleColor prevColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.WriteLine(watch.Elapsed.ToString());
                Console.ForegroundColor = prevColor;
            }
            else
                Console.WriteLine("Invalid problem");
        }

        static void Help()
        {
            Console.WriteLine("help");
            Console.WriteLine("list");
            Console.WriteLine("run <problem number>");
        }

        static void List()
        {
            const ConsoleColor solvedColor = ConsoleColor.DarkGreen;
            const ConsoleColor unsolvedColor = ConsoleColor.DarkRed;
            const ConsoleColor unavailableColor = ConsoleColor.DarkGray;
            ConsoleColor prevColor = Console.ForegroundColor;

            Console.ForegroundColor = solvedColor;
            Console.WriteLine("Solved");
            Console.ForegroundColor = unavailableColor;
            Console.WriteLine("Solved, but lost answer");
            Console.ForegroundColor = unsolvedColor;
            Console.WriteLine("Unsolved");
            Console.ForegroundColor = prevColor;
            Console.WriteLine("-");

            for (int i = 1; i <= probCount; i++)
            {
                if (Assembly.GetExecutingAssembly().GetType("Euler.Solutions.P" + i) != null)
                    Console.ForegroundColor = solvedColor;
                else
                    Console.ForegroundColor = unsolvedColor;

                Console.Write(i.ToString().PadRight(3));
            }

            Console.ForegroundColor = prevColor;
            Console.WriteLine();
        }
    }
}
